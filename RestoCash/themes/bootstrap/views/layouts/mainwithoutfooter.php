<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'fixed'=>'top',
    'fluid'=>false,
    'collapse'=>true,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'Rooms', 'url'=>'#', 'items'=>array(
                    array('label'=>'Show', 'url'=>array('/room/show')),
                    array('label'=>'Admin', 'url'=>array('/room/admin')),
                )),
                array('label'=>'Tables', 'url'=>'#', 'items'=>array(
                    array('label'=>'Admin', 'url'=>array('/table/admin')),
                    array('label'=>'Type', 'url'=>array('/tableType/admin')),
                    array('label'=>'Status', 'url'=>array('/tableStatus/admin')),
                )),
                array('label'=>'Decors', 'url'=>'#', 'items'=>array(
                    array('label'=>'Admin', 'url'=>array('/decor/admin')),
                    array('label'=>'Type', 'url'=>array('/decorType/admin')),
                )),
                array('label'=>'Menu', 'url'=>'#', 'items'=>array(
                    array('label'=>'Item', 'url'=>array('/item/admin')),
                    array('label'=>'Category', 'url'=>array('/category/admin')),
                    array('label'=>'Vat', 'url'=>array('/vat/admin')),
                )),
                array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container-fluid" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>false, 'fade'=>true, 'closeText'=>false), // success, info, warning, error or danger
            'info'=>array('block'=>false, 'fade'=>true, 'closeText'=>false), // success, info, warning, error or danger
            'warning'=>array('block'=>false, 'fade'=>true, 'closeText'=>false), // success, info, warning, error or danger
            'error'=>array('block'=>false, 'fade'=>true, 'closeText'=>false), // success, info, warning, error or danger
            'danger'=>array('block'=>false, 'fade'=>true, 'closeText'=>false), // success, info, warning, error or danger
            ),
        )
    ); ?>

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- page -->

</body>
</html>

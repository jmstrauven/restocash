<?php

/**
 * This is the model class for table "{{order_detail}}".
 *
 * The followings are the available columns in table '{{order_detail}}':
 * @property integer $id
 * @property integer $order
 * @property integer $item
 * @property integer $parent
 * @property integer $category
 * @property integer $vat
 * @property double $price_vat
 * @property double $price
 * @property integer $user
 *
 * The followings are the available model relations:
 * @property Vat $vat0
 * @property Item $item0
 * @property Order $order0
 * @property OrderDetail $parent0
 * @property OrderDetail[] $orderDetails
 * @property User $user0
 */
class OrderDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{order_detail}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order, item, category, vat, user', 'required'),
			array('order, item, parent, category, vat, user', 'numerical', 'integerOnly'=>true),
			array('price_vat, price', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, order, item, parent, category, vat, price_vat, price, user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vat0' => array(self::BELONGS_TO, 'Vat', 'vat'),
			'item0' => array(self::BELONGS_TO, 'Item', 'item'),
			'order0' => array(self::BELONGS_TO, 'Order', 'order'),
			'parent0' => array(self::BELONGS_TO, 'OrderDetail', 'parent'),
			'orderDetails' => array(self::HAS_MANY, 'OrderDetail', 'parent'),
			'user0' => array(self::BELONGS_TO, 'User', 'user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('OrderDetail','ID'),
			'order' => Yii::t('OrderDetail','Order'),
			'item' => Yii::t('OrderDetail','Item'),
			'parent' => Yii::t('OrderDetail','Parent'),
			'category' => Yii::t('OrderDetail','Category'),
			'vat' => Yii::t('OrderDetail','Vat'),
			'price_vat' => Yii::t('OrderDetail','Price Vat'),
			'price' => Yii::t('OrderDetail','Price'),
			'user' => Yii::t('OrderDetail','User'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order',$this->order);
		$criteria->compare('item',$this->item);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('category',$this->category);
		$criteria->compare('vat',$this->vat);
		$criteria->compare('price_vat',$this->price_vat);
		$criteria->compare('price',$this->price);
		$criteria->compare('user',$this->user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

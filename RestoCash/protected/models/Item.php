<?php

/**
 * This is the model class for table "{{item}}".
 *
 * The followings are the available columns in table '{{item}}':
 * @property integer $id
 * @property string $name
 * @property integer $parent
 * @property integer $category
 * @property integer $vat
 * @property double $price
 * @property boolean $disabled
 *
 * The followings are the available model relations:
 * @property Vat $vat0
 * @property Category $category0
 * @property Item $parent0
 * @property Item[] $items
 */
class Item extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{item}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, category, vat', 'required'),
			array('parent, category, vat', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('name', 'length', 'max'=>255),
			array('disabled', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, parent, category, vat, price, disabled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vat0' => array(self::BELONGS_TO, 'Vat', 'vat'),
			'category0' => array(self::BELONGS_TO, 'Category', 'category'),
			'parent0' => array(self::BELONGS_TO, 'Item', 'parent'),
			'items' => array(self::HAS_MANY, 'Item', 'parent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Item','ID'),
			'name' => Yii::t('Item','Name'),
			'parent' => Yii::t('Item','Parent'),
			'category' => Yii::t('Item','Category'),
			'vat' => Yii::t('Item','Vat'),
			'price' => Yii::t('Item','Price'),
			'disabled' => Yii::t('Item','Disabled'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('category',$this->category);
		$criteria->compare('vat',$this->vat);
		$criteria->compare('price',$this->price);
		$criteria->compare('disabled',$this->disabled);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Item the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

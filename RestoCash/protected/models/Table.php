<?php

/**
 * This is the model class for table "{{table}}".
 *
 * The followings are the available columns in table '{{table}}':
 * @property integer $id
 * @property string $name
 * @property integer $x
 * @property integer $y
 * @property boolean $disabled
 * @property integer $type
 * @property integer $status
 * @property integer $room
 * @property integer $client_max
 *
 * The followings are the available model relations:
 * @property Room $room0
 * @property TableStatus $status0
 * @property TableType $type0
 */
class Table extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{table}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('x, y, type, status, room, client_max', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('disabled', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, x, y, disabled, type, status, room, client_max', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'room0' => array(self::BELONGS_TO, 'Room', 'room'),
			'status0' => array(self::BELONGS_TO, 'TableStatus', 'status'),
			'type0' => array(self::BELONGS_TO, 'TableType', 'type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Table','ID'),
			'name' => Yii::t('Table','Name'),
			'x' => Yii::t('Table','X'),
			'y' => Yii::t('Table','Y'),
			'disabled' => Yii::t('Table','Disabled'),
			'type' => Yii::t('Table','Type'),
			'status' => Yii::t('Table','Status'),
			'room' => Yii::t('Table','Room'),
			'client_max' => Yii::t('Table','Client Max'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('x',$this->x);
		$criteria->compare('y',$this->y);
		$criteria->compare('disabled',$this->disabled);
		$criteria->compare('type',$this->type);
		$criteria->compare('status',$this->status);
		$criteria->compare('room',$this->room);
		$criteria->compare('client_max',$this->client_max);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Table the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

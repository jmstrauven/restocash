<?php
/**
 * Created by PhpStorm.
 * User: jms
 * Date: 10/11/13
 * Time: 09:42
 */

class myUser extends User {

    public $password_repeat;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VisitServiceItem the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('firstname, lastname, password_repeat', 'required', 'on' => 'register'),
            array('username', 'unique'),
            array('username', 'length', 'min' => 3, 'max'=>20),
            array('password', 'length', 'min' => 8, 'max'=>32, 'on' => 'register'),
            array('password', 'compare', 'compareAttribute' => 'password_repeat'),
            array('password, password_repeat','safe'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'password_repeat' => 'Password Repeat',
        );
    }


    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            )
        );
    }


    /**
     *
     * @return crypted value
     */
    public function hash($value)
    {
        return crypt($value);
    }

    /**
     * Encrypt password on create and on update: overload beforeSave function
     */
    protected function beforeSave()
    {
        if (parent::beforeSave())
        {
            $this->password = $this->hash($this->password);
            return true;
        }
        return false;
    }

    /**
     * Check if a password value correspond to the stored hashed value
     */
    public function check($value)
    {
        $new_hash = crypt($value, $this->password);
        if ($new_hash == $this->password) {
            return true;
        }
        return false;
    }

} 
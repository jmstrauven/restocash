<?php
/**
* This is the model class for table "{{order_detail}}".
*
*/
class myOrderDetail extends OrderDetail
{
    public $total;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VisitServiceItem the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getTotal() {
        $criteria= new CDbCriteria;
        $criteria->select= 'SUM(price) as total';
        $criteria->condition='`order`='.$this->order0->id;
        $total =  $this::model()->find($criteria);

        return $total->total;

    }


}
<?php
/**
* This is the model class for table "{{decor_type}}".
*
*/
class myDecorType extends DecorType
{


    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VisitServiceItem the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }


}
<?php

class OrderController extends Controller
{
    public $layout='//layouts/column1withoutfooter';

	public function actionIndex()
	{
        if ($id_table = (int)$_GET['idtable']) {
            Yii::app()->session['table'] = $id_table;
            $this->render('index');
        } else {
            Yii::app()->user->setFlash('warning','Please, select a table!');
            $this->redirect(array('room/show'));
        }
	}

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('add','view'),
                'users'=>array('*'),
            ),
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','show'),
                'roles'=>array('owner'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionAdd() {
        if (!Yii::app()->session['table']) {
            Yii::app()->user->setFlash('warning','Please, select a table!');
            $this->redirect(array('room/show'));
        }
        $item = myItem::model()->findByPk($_GET['id']);
        if (!$order = myOrder::model()->find(array('condition'=>'table_id='.Yii::app()->session['table'].' and status <> 7'))) {
            $order = new myOrder();
            $order->table_id = Yii::app()->session['table'];
            $order->status=2;
            $order->user=Yii::app()->user->id;
            $order->create_time = new CDbExpression('NOW()');
            $order->update_time = new CDbExpression('NOW()');
            $order->save();
        } else {
            $order->update_time = new CDbExpression('NOW()');
            $order->save();
        }

        $order_detail = new MyOrderDetail();
        $order_detail->order = $order->id;
        $order_detail->item = $item->id;
        $order_detail->category = $item->category;
        //parent is parent of order_detail and not item !!!
        $order_detail->parent = $item->parent;

        $order_detail->price = $item->price;
        if ($item->price > 0) {
            $order_detail->price_vat = $item->price * ($item->vat0->name/100);
        } else {
            $order_detail->price_vat = 0;
        }
        $order_detail->vat = $item->vat0->name;
        $order_detail->user=Yii::app()->user->id;
        $order_detail->save();

        $order->total = $order_detail->getTotal();
        $order->save();
    }



}
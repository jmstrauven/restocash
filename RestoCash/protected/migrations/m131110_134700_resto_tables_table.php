<?php

class m131110_134700_resto_tables_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{table}}', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'x' => 'int(10) NULL DEFAULT \'100\'',
            'y' => 'int(10) NULL DEFAULT \'100\'',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));

        $this->insert('{{table}}',array('name'=>'chair','x'=>'100','y'=>'100'));
        $this->insert('{{table}}',array('name'=>'rond','x'=>'100','y'=>'100'));
        $this->insert('{{table}}',array('name'=>'square','x'=>'100','y'=>'100'));
        $this->insert('{{table}}',array('name'=>'rectangle top','x'=>'100','y'=>'100'));
        $this->insert('{{table}}',array('name'=>'rectangle long','x'=>'100','y'=>'100'));
        $this->insert('{{table}}',array('name'=>'rond big','x'=>'100','y'=>'100'));

    }

	public function down()
	{
        $this->dropTable('{{table}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
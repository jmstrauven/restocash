<?php

class m131110_232538_create_resto_room_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{room}}', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));

        $this->insert('{{room}}',array('name'=>'Salle'));
        $this->insert('{{room}}',array('name'=>'Terasse'));

        $this->addColumn('{{table}}','room','int DEFAULT NULL');
        $this->addColumn('{{table}}','client_max','int DEFAULT NULL');

        $this->addForeignKey('fk_table_room',
            '{{table}}',
            'room',
            '{{room}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->update('{{table}}',array('room'=>'1','client_max'=>4),"name='rond'");
        $this->update('{{table}}',array('room'=>'1','client_max'=>8),"name='rond big'");
        $this->update('{{table}}',array('room'=>'1','client_max'=>4),"name='square'");
        $this->update('{{table}}',array('room'=>'1','client_max'=>8),"name='rectangle top'");
        $this->update('{{table}}',array('room'=>'1','client_max'=>12),"name='rectangle long'");
        $this->update('{{table}}',array('room'=>'1','client_max'=>1),"name='chair'");

    }

	public function down()
	{
        $this->update('{{table}}',array('room'=>null,'client_max'=>null),"name='rond'");
        $this->update('{{table}}',array('room'=>null,'client_max'=>null),"name='square'");
        $this->update('{{table}}',array('room'=>null,'client_max'=>null),"name='rectangle top'");
        $this->update('{{table}}',array('room'=>null,'client_max'=>null),"name='rectangle long'");
        $this->update('{{table}}',array('room'=>null,'client_max'=>null),"name='rond big'");
        $this->update('{{table}}',array('room'=>null,'client_max'=>null),"name='chair'");

        $this->dropForeignKey('fk_table_room','{{table}}');

        $this->dropColumn('{{table}}','room');
        $this->dropColumn('{{table}}','client_max');

        $this->dropTable('{{room}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
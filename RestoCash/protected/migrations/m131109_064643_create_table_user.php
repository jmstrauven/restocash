<?php

class m131109_064643_create_table_user extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{user}}', array(
            'id' => 'pk',
            'username' => 'string NOT NULL',
            'password' => 'string NOT NULL',
            'email' => 'string NOT NULL',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));
	}

	public function down()
	{
        $this->dropTable('{{user}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
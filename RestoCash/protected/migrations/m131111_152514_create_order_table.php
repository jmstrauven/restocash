<?php

class m131111_152514_create_order_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{order}}', array(
            'id' => 'pk',
            'user' => 'int NOT NULL',
            'table_id' => 'int NOT NULL',
            'create_time' => 'datetime DEFAULT NULL',
            'update_time' => 'datetime DEFAULT NULL',
            'client_quantity' => 'int DEFAULT NULL',
            'status' => 'int NULL DEFAULT \'1\'',
            'total' => 'float NOT NULL DEFAULT \'0\'',
        ));

        $this->addForeignKey('fk_order_user',
            '{{order}}',
            'user',
            '{{user}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey('fk_order_table',
            '{{order}}',
            'table_id',
            '{{table}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey('fk_order_status',
            '{{order}}',
            'status',
            '{{table_status}}', 'id',
            'CASCADE', 'CASCADE'
        );
	}

	public function down()
	{
        $this->dropForeignKey('fk_order_status','{{order}}');
        $this->dropForeignKey('fk_order_table','{{order}}');
        $this->dropForeignKey('fk_order_user','{{order}}');
        $this->dropTable('{{order}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
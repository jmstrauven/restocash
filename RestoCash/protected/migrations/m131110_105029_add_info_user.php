<?php

class m131110_105029_add_info_user extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{user}}','birthdate','date DEFAULT NULL');
	}

	public function down()
	{
        $this->dropColumn('{{user}}','birthdate');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
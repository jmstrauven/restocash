<?php

class m131111_183053_create_language_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{language}}', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'code' => 'string NOT NULL',
        ));

        $this->insert('{{language}}',array('name'=>'English','code'=>'en'));
        $this->insert('{{language}}',array('name'=>'Français','code'=>'fr'));
        $this->insert('{{language}}',array('name'=>'Nederlands','code'=>'nl'));
        $this->insert('{{language}}',array('name'=>'Dutch','code'=>'de'));
	}

	public function down()
	{
        $this->dropTable('{{language}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
<?php

class m131110_145257_update_resto_tables_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{table_type}}', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'css' => 'text NOT NULL',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));

        $this->createTable('{{table_status}}', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'color' => 'string NOT NULL',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));

        $this->insert('{{table_type}}',array('name'=>'Rond','css'=>'-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;behavior: url(/PIE.htc); width: 75px; height: 75px;'));
        $this->insert('{{table_type}}',array('name'=>'Rond Big','css'=>'-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;behavior: url(/PIE.htc); width: 100px; height: 100px;'));
        $this->insert('{{table_type}}',array('name'=>'Square','css'=>'width: 75px; height: 75px;'));
        $this->insert('{{table_type}}',array('name'=>'Rectangle Top','css'=>'width: 75px; height: 150px;'));
        $this->insert('{{table_type}}',array('name'=>'Rectangle Long','css'=>'width: 150px; height: 75px;'));
        $this->insert('{{table_type}}',array('name'=>'Chair','css'=>'width: 40px; height: 40px;'));

        $this->insert('{{table_status}}',array('name'=>'Free', 'color'=>''));
        $this->insert('{{table_status}}',array('name'=>'Used', 'color'=>'info'));
        $this->insert('{{table_status}}',array('name'=>'Apero', 'color'=>'important'));
        $this->insert('{{table_status}}',array('name'=>'Starter', 'color'=>'warning'));
        $this->insert('{{table_status}}',array('name'=>'Main', 'color'=>'inverse'));
        $this->insert('{{table_status}}',array('name'=>'Dessert', 'color'=>'success'));
        $this->insert('{{table_status}}',array('name'=>'Close', 'color'=>''));

        $this->addColumn('{{table}}','type','int NULL DEFAULT \'1\'');
        $this->addColumn('{{table}}','status','int NULL DEFAULT \'1\'');

        $this->addForeignKey('fk_table_type',
            '{{table}}',
            'type',
            '{{table_type}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey('fk_table_status',
            '{{table}}',
            'status',
            '{{table_status}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->update('{{table}}',array('status'=>'1','type'=>'6'),"name='chair'");
        $this->update('{{table}}',array('status'=>'1','type'=>'1'),"name='rond'");
        $this->update('{{table}}',array('status'=>'2','type'=>'2'),"name='rond big'");
        $this->update('{{table}}',array('status'=>'3','type'=>'3'),"name='square'");
        $this->update('{{table}}',array('status'=>'4','type'=>'4'),"name='rectangle top'");
        $this->update('{{table}}',array('status'=>'5','type'=>'5'),"name='rectangle long'");

    }

	public function down()
	{
        $this->update('{{table}}',array('status'=>null,'type'=>null),"name='chair'");
        $this->update('{{table}}',array('status'=>null,'type'=>null),"name='rond'");
        $this->update('{{table}}',array('status'=>null,'type'=>null),"name='square'");
        $this->update('{{table}}',array('status'=>null,'type'=>null),"name='rectangle top'");
        $this->update('{{table}}',array('status'=>null,'type'=>null),"name='rectangle long'");
        $this->update('{{table}}',array('status'=>null,'type'=>null),"name='rond big'");

        $this->dropForeignKey('fk_table_status','{{table}}');
        $this->dropForeignKey('fk_table_type','{{table}}');

        $this->dropColumn('{{table}}','type');
        $this->dropColumn('{{table}}','status');

        $this->dropTable('{{table_type}}');
        $this->dropTable('{{table_status}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
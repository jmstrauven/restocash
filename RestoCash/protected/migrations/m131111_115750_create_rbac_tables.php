<?php

class m131111_115750_create_rbac_tables extends CDbMigration
{

	public function safeUp()
	{
        //$auth = Yii::app()->authManager;
        $db = Yii::app()->db;

        $db->createCommand("CREATE TABLE tbl_AuthItem (
                                    name VARCHAR(64) NOT NULL,
                                    type INTEGER NOT NULL,
                                    description TEXT,
                                    bizrule TEXT,
                                    data TEXT,
                                    PRIMARY KEY (name)) ENGINE = InnoDb")->execute();

        $db->createCommand("CREATE TABLE tbl_AuthItemChild (
                                    parent VARCHAR(64) NOT NULL,
                                    child VARCHAR(64) NOT NULL,
                                    PRIMARY KEY (parent, child),
                                    FOREIGN KEY (parent) REFERENCES tbl_AuthItem (name) ON DELETE CASCADE ON UPDATE CASCADE,
                                    FOREIGN KEY (child) REFERENCES tbl_AuthItem (name) ON DELETE CASCADE ON UPDATE CASCADE
                                    ) ENGINE = InnoDb")->execute();

        $db->createCommand("CREATE TABLE tbl_AuthAssignment (
                                    itemname VARCHAR(64) NOT NULL,
                                    userid VARCHAR(64) NOT NULL,
                                    bizrule TEXT,
                                    data TEXT,
                                    PRIMARY KEY (itemname, userid),
                                    FOREIGN KEY (itemname) REFERENCES tbl_AuthItem (name) ON DELETE CASCADE ON UPDATE CASCADE
                                    ) ENGINE = InnoDb")->execute();

        //$this->insert('{{AuthAssignment}}',array('itemname'=>'owner', 'userid'=>'1'));




    }

	public function safeDown()
	{
        $this->dropTable('tbl_AuthAssignment');
        $this->dropTable('tbl_AuthItemChild');
        $this->dropTable('tbl_AuthItem');
	}
}
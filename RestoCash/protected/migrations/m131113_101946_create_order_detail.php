<?php

class m131113_101946_create_order_detail extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{order_detail}}', array(
            'id' => 'pk',
            'order' => 'int NOT NULL',
            'item' => 'int NOT NULL',
            'parent' => 'int NULL',
            'category' => 'int NOT NULL',
            'vat' => 'float NOT NULL DEFAULT \'0\'',
            'price_vat' => 'float NOT NULL DEFAULT \'0\'',
            'price' => 'float NOT NULL DEFAULT \'0\'',
            'user' => 'int NOT NULL',
        ));

        $this->addForeignKey('fk_order_detail_user',
            '{{order_detail}}',
            'user',
            '{{user}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey('fk_order_detail_order',
            '{{order_detail}}',
            'order',
            '{{order}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey('fk_order_detail_item',
            '{{order_detail}}',
            'item',
            '{{item}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey('fk_order_detail_parent',
            '{{order_detail}}',
            'parent',
            '{{order_detail}}', 'id'
        );

        $db = Yii::app()->db;

        $createTriggerSql = <<< SQL
            CREATE TRIGGER `tigger_update_status_table` AFTER INSERT ON `tbl_order` FOR EACH ROW BEGIN
                UPDATE tbl_table SET status = NEW.status WHERE id = NEW.table_id;
            END;
SQL;

        $this->execute($createTriggerSql);

	}

	public function down()
	{
        $this->execute('DROP TRIGGER /*!50032 IF EXISTS */ `tigger_update_status_table`');
        $this->dropTable('{{order_detail}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
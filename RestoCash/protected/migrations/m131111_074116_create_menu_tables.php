<?php

class m131111_074116_create_menu_tables extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{vat}}', array(
            'id' => 'pk',
            'name' => 'float NOT NULL',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));

        $this->createTable('{{category}}', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'table_status' => 'int NULL DEFAULT \'1\'',
            'orderby' => 'int NULL',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));

        $this->addForeignKey('fk_category_table_status',
            '{{category}}',
            'table_status',
            '{{table_status}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->insert('{{vat}}',array('name'=>'21'));
        $this->insert('{{vat}}',array('name'=>'12.5'));

        $this->insert('{{category}}',array('orderby'=>'1', 'name'=>'Boissons','table_status'=>2, 'disabled'=>0));
        $this->insert('{{category}}',array('orderby'=>'2', 'name'=>'Apéritif','table_status'=>3, 'disabled'=>0));
        $this->insert('{{category}}',array('orderby'=>'3', 'name'=>'Entrées chaudes','table_status'=>4, 'disabled'=>0));
        $this->insert('{{category}}',array('orderby'=>'4', 'name'=>'Entrées froides','table_status'=>4, 'disabled'=>0));
        $this->insert('{{category}}',array('orderby'=>'5', 'name'=>'Viandes','table_status'=>5, 'disabled'=>0));
        $this->insert('{{category}}',array('orderby'=>'6', 'name'=>'Poissons','table_status'=>5, 'disabled'=>0));
        $this->insert('{{category}}',array('orderby'=>'7', 'name'=>'Snack','table_status'=>5, 'disabled'=>0));
        $this->insert('{{category}}',array('orderby'=>'8', 'name'=>'Desserts','table_status'=>6, 'disabled'=>0));
        $this->insert('{{category}}',array('orderby'=>'9', 'name'=>'Boissons chaude','table_status'=>6, 'disabled'=>0));
        $this->insert('{{category}}',array('orderby'=>'10', 'name'=>'Alcools','table_status'=>6, 'disabled'=>0));

        $this->createTable('{{item}}', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'parent' => 'int NULL',
            'category' => 'int NOT NULL',
            'vat' => 'int NOT NULL',
            'price' => 'float NOT NULL DEFAULT \'0\'',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));

        $this->addForeignKey('fk_item_category',
            '{{item}}',
            'category',
            '{{category}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey('fk_item_parent',
            '{{item}}',
            'parent',
            '{{item}}', 'id'
        );

        $this->addForeignKey('fk_item_vat',
            '{{item}}',
            'vat',
            '{{vat}}', 'id',
            'CASCADE', 'CASCADE'
        );

        //$this->insert('{{category}}',array('name'=>'Boissons','status'=>2));
	}

	public function down()
	{

        $this->dropForeignKey('fk_item_vat','{{item}}');
        $this->dropForeignKey('fk_item_parent','{{item}}');
        $this->dropForeignKey('fk_item_category','{{item}}');
        $this->dropTable('{{item}}');


        $this->dropForeignKey('fk_category_table_status','{{category}}');
        $this->dropTable('{{category}}');
        $this->dropTable('{{vat}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
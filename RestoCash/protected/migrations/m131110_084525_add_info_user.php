<?php

class m131110_084525_add_info_user extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{user}}','firstname','string DEFAULT NULL');
        $this->addColumn('{{user}}','lastname','string DEFAULT NULL');
	}

	public function down()
	{
        $this->dropColumn('{{user}}','firstname');
        $this->dropColumn('{{user}}','lastname');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
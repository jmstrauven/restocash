<?php

class m131110_090808_update_user_admin extends CDbMigration
{
	public function up()
	{
        $this->update('{{user}}',array('lastname'=>'Strauven','firstname'=>'Jean-Marc'),"username='grazulex'");
	}

	public function down()
	{
        $this->update('{{user}}',array('lastname'=>NULL,'firstname'=>NULL),"username='grazulex'");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
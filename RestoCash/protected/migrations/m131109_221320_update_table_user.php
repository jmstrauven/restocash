<?php

class m131109_221320_update_table_user extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{user}}','last_login_time','datetime DEFAULT NULL');
        $this->addColumn('{{user}}','create_time','datetime DEFAULT NULL');
        $this->addColumn('{{user}}','update_time','datetime DEFAULT NULL');
	}

	public function down()
	{
        $this->dropColumn('{{user}}','last_login_time');
        $this->dropColumn('{{user}}','create_time');
        $this->dropColumn('{{user}}','update_time');
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
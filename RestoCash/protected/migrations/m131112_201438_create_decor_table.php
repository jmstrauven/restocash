<?php

class m131112_201438_create_decor_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{decor_type}}', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'css' => 'text NOT NULL',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));

        $this->insert('{{decor_type}}',array('name'=>'Flower/tree','css'=>'-moz-border-radius: 25px;-webkit-border-radius: 25px;border-radius: 25px;behavior: url(/PIE.htc); width: 35px; height: 35px; border: 1px solid #e3e3e3; background: #226624;'));
        $this->insert('{{decor_type}}',array('name'=>'Wall Top','css'=>'width: 15px; height: 150px; border: 1px solid #e3e3e3; background: #66630D;'));
        $this->insert('{{decor_type}}',array('name'=>'Wall Long','css'=>'width: 150px; height: 15px; border: 1px solid #e3e3e3; background: #66630D;'));

        $this->createTable('{{decor}}', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'x' => 'int(10) NULL DEFAULT \'100\'',
            'y' => 'int(10) NULL DEFAULT \'100\'',
            'disabled' => 'boolean NOT NULL DEFAULT \'0\'',
        ));

        $this->insert('{{decor}}',array('name'=>'flower 1','x'=>'100','y'=>'100'));
        $this->insert('{{decor}}',array('name'=>'flower 2','x'=>'100','y'=>'100'));
        $this->insert('{{decor}}',array('name'=>'wall top','x'=>'100','y'=>'100'));
        $this->insert('{{decor}}',array('name'=>'wall long','x'=>'100','y'=>'100'));

        $this->addColumn('{{decor}}','type','int NULL DEFAULT \'1\'');

        $this->addForeignKey('fk_decor_type',
            '{{decor}}',
            'type',
            '{{decor_type}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->update('{{decor}}',array('type'=>'1'),"name='flower 1'");
        $this->update('{{decor}}',array('type'=>'1'),"name='flower 2'");
        $this->update('{{decor}}',array('type'=>'2'),"name='wall top'");
        $this->update('{{decor}}',array('type'=>'3'),"name='wall long'");

        $this->addColumn('{{decor}}','room','int DEFAULT NULL');

        $this->addForeignKey('fk_decor_room',
            '{{decor}}',
            'room',
            '{{room}}', 'id',
            'CASCADE', 'CASCADE'
        );

        $this->update('{{decor}}',array('room'=>'1'),"name='flower 1'");
        $this->update('{{decor}}',array('room'=>'1'),"name='flower 2'");
        $this->update('{{decor}}',array('room'=>'1'),"name='wall top'");
        $this->update('{{decor}}',array('room'=>'1'),"name='wall long'");
    }

	public function down()
	{
        $this->dropForeignKey('fk_decor_room','{{decor}}');
        $this->dropForeignKey('fk_decor_type','{{decor}}');

        $this->dropTable('{{decor}}');
        $this->dropTable('{{decor_type}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
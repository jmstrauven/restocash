<?php

/**
 * Main configuration.
 * All properties can be overridden in mode_<mode>.php files
 */

return array(

    // Set yiiPath (relative to Environment.php)
    'yiiPath' => dirname(__FILE__) . '/../../../framework/yii.php',
    'yiicPath' => dirname(__FILE__) . '/../../../framework/yiic.php',
    'yiitPath' => dirname(__FILE__) . '/../../../framework/yiit.php',

    // Set YII_DEBUG and YII_TRACE_LEVEL flags
    'yiiDebug' => true,
    'yiiTraceLevel' => 0,

    // Static function Yii::setPathOfAlias()
    'yiiSetPathOfAlias' => array(
        // uncomment the following to define a path alias
        'bootstrap' => dirname(__FILE__).'/../extensions/bootstrap'
    ),

    // This is the main Web application configuration. Any writable
    // CWebApplication properties can be configured here.
    'configWeb' => array(

        'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'=>'RestoWeb',
        'sourceLanguage'=>'00',
        'language'=>'en',
        'preload'=>array('log'),
        'theme'=>'bootstrap',

        // Autoloading model and component classes
        'import' => array(
            'application.models.*',
            'application.models.*',
            'application.models.my.*',
            'application.components.*',
            'application.extensions.yiidebugtb.*',
        ),

        // Application components
        'components' => array(
            'session' => array (
                'autoStart' => true,
                'class' => 'system.web.CDbHttpSession',
                'connectionID' => 'db',
                'sessionTableName' => 'tbl_session',
            ),
            'bootstrap'=>array(
                'class'=>'bootstrap.components.Bootstrap',
            ),

            'user' => array(
                'allowAutoLogin' => true,
            ),
            'authManager'=>array(
                'class'=>'CDbAuthManager',
                'connectionID'=>'db',
                'defaultRoles'=>array('guest'),
                'itemTable'=>'tbl_AuthItem',
                'itemChildTable'=>'tbl_AuthItemChild',
                'assignmentTable'=>'tbl_AuthAssignment',
            ),
            'urlManager'=>array(
                'urlFormat'=>'path',
                'showScriptName'=>false,
                'rules'=>array(
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
            ),

            'db' => array(
                'connectionString' => '', //override in config/mode_<mode>.php
                'emulatePrepare' => true,
                'username' => '', //override in config/mode_<mode>.php
                'password' => '', //override in config/mode_<mode>.php
                'charset' => 'utf8',
                'tablePrefix' => 'tbl_',
            ),

            // Error handler
            'errorHandler'=>array(
                // use 'site/error' action to display errors
                'errorAction'=>'site/error',
            ),

        ),

        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            // this is used in contact page
            'adminEmail'=>'jms@grazulex.be',
            'translatedLanguages'=>array('fr' => 'Français', 'en' => 'English'),
            'defaultLanguage' => 'uk',
        ),

    ),

    // This is the Console application configuration. Any writable
    // CConsoleApplication properties can be configured here.
    // Leave array empty if not used.
    // Use value 'inherit' to copy from generated configWeb.
    'configConsole' => array(

        'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'=>'RestoWeb',
        'sourceLanguage'=>'00',
        'language'=>'en',
        'preload'=>array('log'),

        // Autoloading model and component classes
        'import'=>'inherit',

        // Application componentshome
        'components'=>array(

            // Database
            'db'=>'inherit',

            // Application Log
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    // Save log messages on file
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'error, warning, trace, info',
                    ),
                ),
            ),

        ),

    ),

);
<?php

return array(
    'yiiDebug' => true,
    'yiiTraceLevel' => 3,
    'yiiSetPathOfAlias' => array(
    ),

    'configWeb' => array(
        'modules' => array(
            'gii' => array(
                'generatorPaths'=>array(
                    'bootstrap.gii',
                ),
                'class' => 'system.gii.GiiModule',
                'password' => false,
            ),
        ),
        'components' => array(

            'cache'=>array(
                'class'=>'CFileCache',
            ),

            // Asset manager
            'assetManager' => array(
                'linkAssets' => true, // publish symbolic links for easier developing
            ),

            // Database
            'db' => array(
                'connectionString' => 'mysql:host=127.0.0.1;dbname=restocash',
                'username' => 'restocash',
                'password' => 'dOAFIH8Lcj',
                'enableParamLogging' => true,
                'enableProfiling'=>true,

            ),

            // Application Log
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    // Save log messages on file
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'error, warning, trace, info',
                    ),
                    array( // configuration for the toolbar
                        'class'=>'XWebDebugRouter',
                        'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle',
                        'levels'=>'error, warning, trace, profile, info',
                        'allowedIPs'=>array('127.0.0.1','::1','192.168.1.54','192\.168\.1[0-5]\.[0-9]{3}'),
                    ),
                ),
            ),

        ),

    ),

    // This is the Console application configuration. Any writable
    // CConsoleApplication properties can be configured here.
    // Leave array empty if not used.
    // Use value 'inherit' to copy from generated configWeb.
    'configConsole' => array(
        'components' => array(
            'db' => 'inherit',

            'authManager'=>array(
                'class'=>'CDbAuthManager',
                'connectionID'=>'db',
                'defaultRoles'=>array('authenticated', 'guest'),
                'itemTable'=>'tbl_AuthItem',
                'itemChildTable'=>'tbl_AuthItemChild',
                'assignmentTable'=>'tbl_AuthAssignment',
            ),
        ),
    ),

);
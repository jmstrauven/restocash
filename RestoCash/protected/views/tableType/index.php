<?php
$this->breadcrumbs=array(
	'Table Types',
);

$this->menu=array(
	array('label'=>'Create TableType','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Manage TableType','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Table Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

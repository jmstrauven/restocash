<?php
$this->breadcrumbs=array(
	'Table Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TableType','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Manage TableType','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Create TableType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
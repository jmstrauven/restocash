<?php
$this->breadcrumbs=array(
	'Table Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TableType','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create TableType','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'View TableType','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
	array('label'=>'Manage TableType','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Update TableType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
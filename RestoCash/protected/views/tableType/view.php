<?php
$this->breadcrumbs=array(
	'Table Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TableType','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create TableType','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Update TableType','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Delete TableType','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'remove-circle'),
	array('label'=>'Manage TableType','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>View TableType #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
    'type'=>'striped bordered condensed',
	'attributes'=>array(
		'id',
		'name',
		'css',
		'disabled',
	),
)); ?>

<?php
$this->breadcrumbs=array(
	'Decor Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DecorType','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Manage DecorType','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Create DecorType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Decor Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DecorType','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create DecorType','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'View DecorType','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
	array('label'=>'Manage DecorType','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Update DecorType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
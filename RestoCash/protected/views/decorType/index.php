<?php
$this->breadcrumbs=array(
	'Decor Types',
);

$this->menu=array(
	array('label'=>'Create DecorType','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Manage DecorType','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Decor Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

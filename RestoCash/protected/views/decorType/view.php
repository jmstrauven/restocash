<?php
$this->breadcrumbs=array(
	'Decor Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DecorType','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create DecorType','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Update DecorType','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Delete DecorType','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'remove-circle'),
	array('label'=>'Manage DecorType','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>View DecorType #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
    'type'=>'striped bordered condensed',
	'attributes'=>array(
		'id',
		'name',
		'css',
		'disabled',
	),
)); ?>

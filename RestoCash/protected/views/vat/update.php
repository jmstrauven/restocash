<?php
$this->breadcrumbs=array(
	'Vats'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Vat','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Vat','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'View Vat','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
	array('label'=>'Manage Vat','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Update Vat <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
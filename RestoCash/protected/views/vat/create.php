<?php
$this->breadcrumbs=array(
	'Vats'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Vat','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Manage Vat','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Create Vat</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Rooms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Room','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Manage Room','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Create Room</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
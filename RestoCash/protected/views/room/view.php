<?php
$this->breadcrumbs=array(
	'Rooms'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Room','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Room','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Update Room','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Delete Room','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'remove-circle'),
	array('label'=>'Manage Room','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>View Room #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
    'type'=>'striped bordered condensed',
	'attributes'=>array(
		'id',
		'name',
		'disabled',
	),
)); ?>

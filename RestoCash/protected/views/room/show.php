<?php
/* @var $this RoomsController */

$this->widget('bootstrap.widgets.TbMenu', array(
    'type'=>'pills', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>$this->getRooms(),
));


$str_js = "
$(function() {
    $( '#room div' ).draggable({
        stack: '#room div',
        stop: function(event, ui) {
            var pos_x = ui.offset.left;
            var pos_y = ui.offset.top;
            var need = ui.helper.data('need');
            var type = ui.helper.data('type');

            $.ajax({
                type: 'POST',
                url: 'savepos',
                data: { x: pos_x, y: pos_y, need_id: need, type_id: type}
            })
        }
    });
});
";
Yii::app()->clientScript->registerScript('sortable-project', $str_js);
?>

<div id="room" style="width: 100%;">
    <?php
    if ($decos = myDecor::model()->findAll('room=:room AND disabled=:disabled', array(':room'=>Yii::app()->session['room'],':disabled'=>0))) {
        foreach ($decos as $deco) {
            $this->beginWidget('zii.widgets.jui.CJuiDraggable',array(
                'options'=>array(
                    'opacity' => 0.35,
                    'cursor'=>'move',
                ),
                'htmlOptions'=>array(
                    'style'=>$deco->type0->css. 'left:'.$deco->x.'px; top:'.$deco->y.'px; padding: 5px; position: absolute; text-align: center;',
                    'data-need'=>$deco->id,
                    'data-type'=>2,
                ),
                'id'=>$deco->id,
            ));
            $this->endWidget();
        }
    }


    if ($tables = myTable::model()->findAll('room=:room AND disabled=:disabled', array(':room'=>Yii::app()->session['room'],':disabled'=>0))) {
        foreach ($tables as $table) {
            $this->beginWidget('zii.widgets.jui.CJuiDraggable',array(
                'options'=>array(
                    'opacity' => 0.35,
                    'cursor'=>'move',
                ),
                'htmlOptions'=>array(
                    'style'=>$table->type0->css. 'left:'.$table->x.'px; top:'.$table->y.'px; padding: 5px; border: 1px solid #e3e3e3; background: #F4F4F4;position: absolute; text-align: center;',
                    'data-need'=>$table->id,
                    'data-type'=>1,
                ),
                'id'=>$table->id,
            ));

            $this->widget('bootstrap.widgets.TbBadge', array(
                'label'=>$table->id,
            ));


            $this->widget('bootstrap.widgets.TbLabel', array(
                'type'=>$table->status0->color,
                'label'=>$table->status0->name,
            ));
            if ((int)$table->type0->id != 6) {
                echo '</br>';

                echo '<i class="icon-user"> </i>';
                if ($table->status0->id != 1) {
                    $this->widget('bootstrap.widgets.TbBadge', array(
                        'type'=>'success',
                        'label'=>'1/'.$table->client_max,
                    ));
                } else {
                    $this->widget('bootstrap.widgets.TbBadge', array(
                        'type'=>'success',
                        'label'=>$table->client_max,
                    ));
                }
            }


            if ($table->status0->id != 1) {
                $startTime = myOrder::model()->find(array('condition'=>'table_id='.$table->id.' and status <> 7'))->create_time;
                echo '</br>';
                echo '<i class="icon-time"> </i>';
                $this->widget('bootstrap.widgets.TbBadge', array(
                    'type'=>'important',
                  //  'label'=>  date_diff($startTime, new DateTime("now"))
                ));
            }

            echo '</br>';
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'link',
                'label'=>'Open',
                'type'=>'info',
                'url'=>$this->createUrl('order/index',array('idtable'=>$table->id)),
                'size'=>'mini',
            ));

            $this->endWidget();
        }
    }




?>
</div>

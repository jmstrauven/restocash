<?php
$this->breadcrumbs=array(
	'Table Statuses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TableStatus','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create TableStatus','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'View TableStatus','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
	array('label'=>'Manage TableStatus','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Update TableStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
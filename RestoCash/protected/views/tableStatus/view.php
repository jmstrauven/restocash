<?php
$this->breadcrumbs=array(
	'Table Statuses'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TableStatus','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create TableStatus','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Update TableStatus','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Delete TableStatus','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'remove-circle'),
	array('label'=>'Manage TableStatus','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>View TableStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
    'type'=>'striped bordered condensed',
	'attributes'=>array(
		'id',
		'name',
		'color',
		'disabled',
	),
)); ?>

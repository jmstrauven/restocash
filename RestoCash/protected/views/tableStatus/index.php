<?php
$this->breadcrumbs=array(
	'Table Statuses',
);

$this->menu=array(
	array('label'=>'Create TableStatus','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Manage TableStatus','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Table Statuses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
$this->breadcrumbs=array(
	'Table Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TableStatus','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Manage TableStatus','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Create TableStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
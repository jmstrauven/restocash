<?php
$this->breadcrumbs=array(
	'Items',
);

$this->menu=array(
	array('label'=>'Create Item','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Manage Item','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Items</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

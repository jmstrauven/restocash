<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->label($model, 'parent'); ?>
    <?php echo $form->dropDownList($model,'parent', CHtml::listData(myItem::model()->findAll(),
            'id',
            'name'
        ),array('class'=>'span5')
    ); ?>

    <?php echo $form->label($model, 'category'); ?>
    <?php echo $form->dropDownList($model,'category', CHtml::listData(myCategory::model()->findAll(),
            'id',
            'name'
        ),array('class'=>'span5')
    ); ?>

    <?php echo $form->label($model, 'vat'); ?>
    <?php echo $form->dropDownList($model,'vat', CHtml::listData(myVat::model()->findAll(),
            'id',
            'name'
        ),array('class'=>'span5')
    ); ?>

	<?php echo $form->textFieldRow($model,'price',array('class'=>'span5')); ?>

	<?php echo $form->checkBoxRow($model,'disabled'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php
$this->breadcrumbs=array(
	'Items'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Item','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Item','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'View Item','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
	array('label'=>'Manage Item','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Update Item <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
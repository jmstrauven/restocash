<?php
$this->breadcrumbs=array(
	'Items'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Item','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Item','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Update Item','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Delete Item','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'remove-circle'),
	array('label'=>'Manage Item','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>View Item #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
    'type'=>'striped bordered condensed',
	'attributes'=>array(
		'id',
		'name',
		'parent0.name',
		'category0.name',
		'vat0.name',
		'price',
		'disabled',
	),
)); ?>

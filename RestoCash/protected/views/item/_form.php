<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'item-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->label($model, 'parent'); ?>
    <?php
    $data = CMap::mergeArray(array(null=>''),CHtml::listData(myItem::model()->findAll(),'id','name') );
    echo $form->dropDownList($model,'parent', $data,array('class'=>'span5')
    ); ?>

    <?php echo $form->label($model, 'category'); ?>
    <?php echo $form->dropDownList($model,'category', CHtml::listData(myCategory::model()->findAll(),
            'id',
            'name'
        ),array('class'=>'span5')
    ); ?>

    <?php echo $form->label($model, 'vat'); ?>
    <?php echo $form->dropDownList($model,'vat', CHtml::listData(myVat::model()->findAll(),
            'id',
            'name'
        ),array('class'=>'span5')
    ); ?>

	<?php echo $form->textFieldRow($model,'price',array('class'=>'span5')); ?>

	<?php echo $form->checkBoxRow($model,'disabled'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

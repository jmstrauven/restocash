<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Category','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Category','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Update Category','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Delete Category','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'remove-circle'),
	array('label'=>'Manage Category','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>View Category #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
    'type'=>'striped bordered condensed',
	'attributes'=>array(
		'id',
		'name',
		'tableStatus.name',
		'disabled',
	),
)); ?>

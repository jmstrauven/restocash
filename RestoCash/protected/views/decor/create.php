<?php
$this->breadcrumbs=array(
	'Decors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Decor','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Manage Decor','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Create Decor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
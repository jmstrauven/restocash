<?php
$this->breadcrumbs=array(
	'Decors'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Decor','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Decor','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'View Decor','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
	array('label'=>'Manage Decor','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Update Decor <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'decor-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>


    <?php echo $form->label($model, 'type'); ?>
    <?php echo $form->dropDownList($model,'type', CHtml::listData(myDecorType::model()->findAll(),
            'id',
            'name'
        ),array('class'=>'span5')
    ); ?>

    <?php echo $form->label($model, 'room'); ?>
    <?php echo $form->dropDownList($model,'room', CHtml::listData(myRoom::model()->findAll(),
            'id',
            'name'
        ),array('class'=>'span5')
    ); ?>

    <?php echo $form->checkBoxRow($model,'disabled'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

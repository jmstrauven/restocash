<?php
$this->breadcrumbs=array(
	'Decors'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Decor','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Decor','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Update Decor','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Delete Decor','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'remove-circle'),
	array('label'=>'Manage Decor','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>View Decor #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
    'type'=>'striped bordered condensed',
	'attributes'=>array(
		'id',
		'name',
        'type0.name',
        'room0.name',
		'disabled',
	),
)); ?>

<?php
$this->breadcrumbs=array(
	'Decors',
);

$this->menu=array(
	array('label'=>'Create Decor','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Manage Decor','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Decors</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

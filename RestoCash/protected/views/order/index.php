<?php
/* @var $this OrderController */

$this->breadcrumbs=array(
	'Order',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>
<?php
if ($categories = myCategory::model()->findAll(array('condition'=>'disabled=0', 'order' => 'orderby ASC'))) {
    echo '<ul>';
        foreach ($categories as $category) {
            echo "<li>".$category->name."</li>";
            if ($items = myItem::model()->findAll(array('condition'=>'disabled=0 and category = '.$category->id.' and isnull(parent)', 'order' => 'name ASC'))) {
                echo '<ul>';
                foreach ($items as $item) {
                    echo "<li>". CHtml::ajaxLink($item->name,'add/id/'.$item->id)."</li>";
                    if ($options = myItem::model()->findAll(array('condition'=>'disabled=0 and category = '.$category->id.' and parent = '.$item->id, 'order' => 'name ASC'))) {
                        echo '<ul>';
                        foreach ($options as $option) {
                            echo "<li>". CHtml::ajaxLink($option->name,'add/id/'.$option->id)."</li>";
                        }
                        echo '</ul>';
                    }
                }
                echo '</ul>';
            }
        }
    echo '</ul>';
}

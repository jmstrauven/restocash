<?php
$this->breadcrumbs=array(
	'Tables'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Table','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Table','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'View Table','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
	array('label'=>'Manage Table','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Update Table <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
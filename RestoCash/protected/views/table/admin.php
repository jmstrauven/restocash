<?php
$this->breadcrumbs=array(
	'Tables'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Table','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Table','url'=>array('create'),'icon'=>'plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('table-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tables</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'table-grid',
    'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'name',
		'type0.name',
        'room0.name',
        'client_max',
        'disabled',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>

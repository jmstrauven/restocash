<?php
$this->breadcrumbs=array(
	'Tables'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Table','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Manage Table','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Create Table</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
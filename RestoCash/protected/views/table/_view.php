<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type0->name); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('room')); ?>:</b>
    <?php echo CHtml::encode($data->room0->name); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('client_max')); ?>:</b>
    <?php echo CHtml::encode($data->client_max); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('disabled')); ?>:</b>
    <?php echo CHtml::encode($data->disabled); ?>
    <br />


</div>
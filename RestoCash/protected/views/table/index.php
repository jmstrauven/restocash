<?php
$this->breadcrumbs=array(
	'Tables',
);

$this->menu=array(
	array('label'=>'Create Table','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Manage Table','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>Tables</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

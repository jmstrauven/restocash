<?php
$this->breadcrumbs=array(
	'Tables'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Table','url'=>array('index'),'icon'=>'book'),
	array('label'=>'Create Table','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Update Table','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
	array('label'=>'Delete Table','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'remove-circle'),
	array('label'=>'Manage Table','url'=>array('admin'),'icon'=>'user'),
);
?>

<h1>View Table #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
    'type'=>'striped bordered condensed',
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'type0.name',
        'room0.name',
        'client_max',
        'disabled',
	),
)); ?>
